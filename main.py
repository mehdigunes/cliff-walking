from MyCell import MyCell
import random as random
import sys
import argparse

def parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--setting", help="q for QLearning sarsa for SARSA", type=str)
    parser.add_argument("-r", "--runs", help="Amount of runs", type=int)
    parser.add_argument("-e", "--epsilon", help="Probability of e-Greedy", type=float)
    parser.add_argument("-a", "--alpha", help="Alpha value", type=float)
    parser.add_argument("-g", "--gamma", help="Gamma value", type=float)

    return parser

def main():
    args = parser().parse_args()
    setting = args.setting
    runs = args.runs
    epsilon = args.epsilon
    alpha = args.alpha
    gamma = args.gamma

    startPos = [4, 0]
    grid = makeGrid(5, 10)
    grid = fillGrid(grid)

    if setting == "q":
        grid = qlearn(grid, startPos, runs, epsilon, alpha, gamma)
    else:
        grid = sarsa(grid, startPos, runs, epsilon, alpha, gamma)
    showGrid(grid)
    showBestValueGrid(grid)

# Create an empty grid of rows x columns
def makeGrid(rows, columns):
    grid = [[] for x in range(rows)]
    
    for row in grid:
        for i in range(columns):
            cell = MyCell(-1, False)
            row.append(cell)

    return grid

# Fills grid with problem specific settings
def fillGrid(grid):
    for i in range(1, 10):
        grid[4][i].termination = True

    for i in range(1, 9):
        grid[4][i].reward = -100

    grid[4][9].reward = 10

    return grid

# Prints the grid
def showGrid(grid):
    print("-"*200)
    for row in grid:
        message1 = "|"
        message2 = "|"
        for cell in row:
            up = cell.up
            down = cell.down
            left = cell.left
            right = cell.right

            message1 += "U:" + "{:^7.2f}".format(up) + " D:" + "{:^7.2f}|".format(down)
            message2 += "L:" + "{:^7.2f}".format(left) + " R:" + "{:^7.2f}|".format(right)
        print(message1)
        print(message2)
        print("-"*200)

def showBestValueGrid(grid):
    print("-"*100)
    for row in grid:
        message = "|"
        for cell in row:
            row1 = grid.index(row)
            column1 = row.index(cell)
            [row2, column2] = findMove(grid, [grid.index(row), row.index(cell)], True)
            if (row1 == row2 - 1): # Down
                message += "D:" + "{:^7.2f}|".format(grid[row1][column1].down)
            if (row1 == row2 + 1): # Up
                message += "U:" + "{:^7.2f}|".format(grid[row1][column1].up)
            if (column1 == column2 - 1): # Right
                message += "R:" + "{:^7.2f}|".format(grid[row1][column1].right)
            if (column1 == column2 + 1): # Left
                message += "L:" + "{:^7.2f}|".format(grid[row1][column1].left)
        print(message)
        print("-"*100)

# Returns next move's position
def findMove(grid, pos, best):
    [row, column] = pos
    possible_moves = []
    q_values = []

    if (row - 1 >= 0):
        possible_moves.append([row - 1, column])
        q_values.append(grid[row][column].up)

    if (row + 1 <= 4):
        possible_moves.append([row + 1, column])
        q_values.append(grid[row][column].down)

    if (column + 1 <= 9):
        possible_moves.append([row, column + 1])
        q_values.append(grid[row][column].right)
        
    if (column - 1 >= 0):
        possible_moves.append([row, column - 1])
        q_values.append(grid[row][column].left)

    if best:
        nextMove = possible_moves[q_values.index(max(q_values))]
    else:
        nextMove = possible_moves[random.randint(0, len(possible_moves) - 1)]

    return nextMove

def qlearn(grid, pos, runs, epsilon, alpha, gamma):
    startPos = pos

    for i in range(0, runs):
        pos = startPos

        while not grid[pos[0]][pos[1]].termination:

            s1 = grid[pos[0]][pos[1]]
            
            if (random.uniform(0, 1) < epsilon):
                nextMove = findMove(grid, pos, False)
            else:
                nextMove = findMove(grid, pos, True)

            s2 = grid[nextMove[0]][nextMove[1]]

            [row1, column1] = pos
            [row2, column2] = nextMove
            # print(nextMove)

            qValueS2 = maxQValue(s2)

            if (row1 == row2 - 1): # Down
                grid[pos[0]][pos[1]].down = s1.down + alpha * (s2.reward + gamma * qValueS2 - s1.down)
            elif (row1 == row2 + 1): # Up
                grid[pos[0]][pos[1]].up = s1.up + alpha * (s2.reward + gamma * qValueS2 - s1.up)
            elif (column1 == column2 - 1): # Right
                grid[pos[0]][pos[1]].right = s1.right + alpha * (s2.reward + gamma * qValueS2 - s1.right)
            elif (column1 == column2 + 1): # Left
                grid[pos[0]][pos[1]].left = s1.left + alpha * (s2.reward + gamma * qValueS2 - s1.left)

            pos = nextMove

    return grid

def sarsa(grid, pos, runs, epsilon, alpha, gamma):
    startPos = pos

    for i in range(0, runs):
        pos = startPos

        if (random.uniform(0, 1) < epsilon):
            nextMove = findMove(grid, pos, False)
        else:
            nextMove = findMove(grid, pos, True)

        while not grid[pos[0]][pos[1]].termination:
            s1 = grid[pos[0]][pos[1]]
            
            if (random.uniform(0, 1) < epsilon):
                secondMove = findMove(grid, nextMove, False)
            else:
                secondMove = findMove(grid, nextMove, True)

            s2 = grid[nextMove[0]][nextMove[1]]

            [row1, column1] = pos
            [row2, column2] = nextMove
            [row3, column3] = secondMove

            if (row2 == row3 - 1): # Down
                qValueS2 = s2.down
            elif (row2 == row3 + 1): # Up
                qValueS2 = s2.up
            elif (column2 == column3 - 1): # Right
                qValueS2 = s2.right
            elif (column2 == column3 + 1): # Left
                qValueS2 = s2.left

            if (row1 == row2 - 1): # Down
                grid[pos[0]][pos[1]].down = s1.down + alpha * (s2.reward + gamma * qValueS2 - s1.down)
            elif (row1 == row2 + 1): # Up
                grid[pos[0]][pos[1]].up = s1.up + alpha * (s2.reward + gamma * qValueS2 - s1.up)
            elif (column1 == column2 - 1): # Right
                grid[pos[0]][pos[1]].right = s1.right + alpha * (s2.reward + gamma * qValueS2 - s1.right)
            elif (column1 == column2 + 1): # Left
                grid[pos[0]][pos[1]].left = s1.left + alpha * (s2.reward + gamma * qValueS2 - s1.left)

            pos = nextMove
            nextMove = secondMove

    return grid

def maxQValue(s):
    qValues = [s.up, s.down, s.left, s.right]
    return max(qValues)

main()