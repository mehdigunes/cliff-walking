class MyCell:
    reward = 0
    up = 0
    down = 0
    left = 0
    right = 0
    termination = False

    def __init__(self, r, t):
        self.reward = r
        self.up = 0
        self.down = 0
        self.left = 0
        self.right = 0
        self.termination = t

    def info(self):
        message = "Reward: " + str(self.reward)
        message += " qValue: " + str(self.up) + " " + str(self.down) + " " + str(self.left) + " " + str(self.right) + " "
        message += " Termination: " + "Yes" if self.termination == True else "No"
        return message